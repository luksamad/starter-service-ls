package gov.va.mobile.starter.v1.service.config;

import gov.va.vamf.service.v1.EnvVarUtil;
import gov.va.vamf.service.v1.Sanitizer;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

/**
 * Environment Properties Configuration. The properties are initialized from the Runtime System Environment, using the
 * {@link #afterPropertiesSet()} method of this {@link InitializingBean}.
 *
 * @see InitializingBean
 * @since 1.0
 */
@Log4j2
@Getter
@Configuration
public class EnvConfig implements InitializingBean {

    private static final String USER_SESSION_SVC_V1_URL = "USER_SESSION_SVC_V1_URL";
    private static final String MOBILE_MVI_SERVICE_URL = "MOBILE_MVI_SERVICE_URL";

    private static final String NO_VALUE_PROVIDED_FOR_ENVIRONMENT_VARIABLE = "No value provided for environment variable: ";

    private String userSessionServiceUrl;
    private String mobileMviServiceUrl;

    @Override
    public void afterPropertiesSet() {
        loadConfiguration();
        validateConfiguration();
        logConfiguration();
    }

    /**
     * Loads the environment configuration parameters from the current runtime environment.
     */
    private void loadConfiguration() {
        userSessionServiceUrl = EnvVarUtil.getEnvVar(USER_SESSION_SVC_V1_URL, null);
        mobileMviServiceUrl = EnvVarUtil.getEnvVar(MOBILE_MVI_SERVICE_URL, null);
    }

    /**
     * Validates the required environment configuration parameters are properly initialized.
     */
    private void validateConfiguration() {
        if (StringUtils.isEmpty(mobileMviServiceUrl)) {
            throw new IllegalArgumentException(NO_VALUE_PROVIDED_FOR_ENVIRONMENT_VARIABLE + MOBILE_MVI_SERVICE_URL);
        }
        if (StringUtils.isEmpty(userSessionServiceUrl)) {
            throw new IllegalArgumentException(NO_VALUE_PROVIDED_FOR_ENVIRONMENT_VARIABLE + USER_SESSION_SVC_V1_URL);
        }
    }

    /**
     * Logs non-sensitive environment configuration after initialization.
     */
    private void logConfiguration() {
        if (log.isInfoEnabled()) {
            log.info("MOBILE_MVI_SERVICE_URL: {}", Sanitizer.sanitize(mobileMviServiceUrl));
            log.info("USER_SESSION_SVC_V1_URL: {}", Sanitizer.sanitize(userSessionServiceUrl));
        }
    }
}