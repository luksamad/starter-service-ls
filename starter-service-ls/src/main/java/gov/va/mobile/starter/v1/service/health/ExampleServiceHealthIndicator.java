package gov.va.mobile.starter.v1.service.health;

import gov.va.mobile.starter.v1.service.config.EnvConfig;
import gov.va.vamf.service.v1.Sanitizer;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
@Component
public class ExampleServiceHealthIndicator extends AbstractHealthIndicator {

    private static final String USER_SESSION_SERVICE_NAME = "User Session Service";
    private static final String MOBILE_MVI_SERVICE_NAME = "MVI Service";

    private final RestTemplate restTemplate;
    private final EnvConfig envConfig;
    private Map<String, String> serviceDataMap;

    public ExampleServiceHealthIndicator(final RestTemplate restTemplate, final EnvConfig envConfig) {
        this.restTemplate = restTemplate;
        this.envConfig = envConfig;
    }

    @Override
    public void doHealthCheck(final Health.Builder builder) {
        final AtomicInteger downCount = new AtomicInteger(0);
        setServiceDataMap(getServiceDataMap(this.envConfig));
        serviceDataMap.forEach((serviceName, url) -> {
            validateServiceData(serviceName, "serviceName");
            validateServiceData(url, "url");
            if (getStatus(serviceName, url)) {
                downCount.set(downCount.get() + 1);
            }
        });

        if (downCount.get() > 0 && downCount.get() < 2) {
            builder.up();
        } else if (downCount.get() == 2) {
            builder.down();
        } else {
            builder.up();
        }
    }


    private boolean getStatus(final String serviceName, final String url) {

        int statusCode = 0;
        String responseBody = "";
        ResponseEntity<String> response = null;

        try {
            response = this.restTemplate.getForEntity(url, String.class);
            if (response != null) {
                statusCode = response.getStatusCodeValue();
                responseBody = response.getBody();
            }
        } catch (RestClientResponseException e) {
            statusCode = e.getRawStatusCode();
            responseBody = e.getResponseBodyAsString();
        }

        if (response == null) {
            if (log.isErrorEnabled()) {
                log.error("received an error from {}; HTTP status {}, body: {}", Sanitizer.sanitize(serviceName), statusCode, Sanitizer.sanitize(responseBody));
            }
            return true;
        } else {
            if (response.getStatusCode() == HttpStatus.OK) {
                return false;
            } else {
                if (log.isErrorEnabled()) {
                    log.error("received an error from {}; HTTP status {}, body: {}", Sanitizer.sanitize(serviceName), statusCode, Sanitizer.sanitize(responseBody));
                }
                return true;
            }
        }
    }


    public void setServiceDataMap(final Map<String, String> serviceDataMap) {
        this.serviceDataMap = serviceDataMap;
    }

    private Map<String, String> getServiceDataMap(final EnvConfig config) {
        Map<String, String> serviceDataMap = new ConcurrentHashMap<>();
        serviceDataMap.put(USER_SESSION_SERVICE_NAME, config.getUserSessionServiceUrl());
        serviceDataMap.put(MOBILE_MVI_SERVICE_NAME, config.getMobileMviServiceUrl());
        return serviceDataMap;
    }

    private void validateServiceData(final String field, final String type) {
        if (field == null) {
            throw new IllegalStateException(type + " must not be null");
        }
    }
}