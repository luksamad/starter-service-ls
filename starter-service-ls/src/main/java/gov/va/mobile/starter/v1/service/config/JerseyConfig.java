package gov.va.mobile.starter.v1.service.config;

import gov.va.mobile.starter.v1.service.resources.ServiceResource;
import gov.va.mobile.starter.v1.service.resources.SessionVarResource;
import gov.va.vamf.security.v1.filters.JwtAuthResourceFilter;
import gov.va.vamf.security.v1.filters.JwtAuthRoleFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.autoconfigure.jersey.ResourceConfigCustomizer;
import org.springframework.context.annotation.Configuration;

/**
 * {@link ResourceConfigCustomizer} implementation for configuring custom Jersey Endpoints, ExceptionMappers, Filters,
 * and Features.
 *
 * @see ResourceConfigCustomizer
 * @see ResourceConfig
 * @since 1.0
 */
@Configuration
public class JerseyConfig implements ResourceConfigCustomizer {

    @Override
    public void customize(final ResourceConfig config) {
        config.register(JwtAuthResourceFilter.class);
        config.register(JwtAuthRoleFilter.class);

        config.register(ServiceResource.class);
        config.register(SessionVarResource.class);
    }
}