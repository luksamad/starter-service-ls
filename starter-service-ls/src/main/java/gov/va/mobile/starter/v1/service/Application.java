package gov.va.mobile.starter.v1.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main application class for starter-service-ls.
 *
 * @since 1.0
 */
@SpringBootApplication
public class Application {

    /**
     * Starts the Spring Boot application.
     *
     * @param args arguments for this app
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}