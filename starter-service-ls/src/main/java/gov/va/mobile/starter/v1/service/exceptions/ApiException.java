package gov.va.mobile.starter.v1.service.exceptions;

public class ApiException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final ErrorCode errorCode;

    public ApiException(final ErrorCode errorCode, final Throwable cause) {
        this(errorCode, cause, null);
    }

    public ApiException(final ErrorCode errorCode, final Throwable cause, final String detail) {
        super(String.format("Error code %d: %s %s (upstream: none)", errorCode.value(), errorCode.getMessage(),
                detail), cause);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}