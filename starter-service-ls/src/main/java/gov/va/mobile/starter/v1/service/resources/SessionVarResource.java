package gov.va.mobile.starter.v1.service.resources;

import gov.va.mobile.starter.v1.client.model.SessionVar;
import gov.va.mobile.starter.v1.service.proxies.SessionServiceClient;
import gov.va.vamf.security.v1.VamfJwtClaimsConstants;
import gov.va.vamf.security.v1.filters.JwtRbacRestricted;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;


@Component
@Path("/user/sessionvar")
@JwtRbacRestricted(VamfJwtClaimsConstants.VAMF_ROLE_STAFF)
public class SessionVarResource {

    private final SessionServiceClient sessionServiceClient;

    public SessionVarResource(final SessionServiceClient sessionServiceClient) {
        this.sessionServiceClient = sessionServiceClient;
    }

    @GET
    @Path("/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSessionVariable(@PathParam("key") final String key, @Context final HttpHeaders headers) {
        final String jwt = headers.getHeaderString(VamfJwtClaimsConstants.VAMF_JWT_HEADER);
        return sessionServiceClient.getSessionVariable(jwt, key);
    }

    @PUT
    @Path("/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    public void updateSessionVariable(@PathParam("key") final String key, @RequestBody final SessionVar sessionVar, @Context final HttpHeaders headers) {
        final String jwt = headers.getHeaderString(VamfJwtClaimsConstants.VAMF_JWT_HEADER);
        sessionServiceClient.putSessionVariable(jwt, key, sessionVar.getValue());
    }

}