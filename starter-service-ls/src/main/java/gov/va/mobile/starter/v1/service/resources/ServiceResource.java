package gov.va.mobile.starter.v1.service.resources;

import gov.va.mobile.starter.v1.client.model.PatientInfo;
import gov.va.mobile.starter.v1.client.model.PatientInfoResponse;
import gov.va.vamf.security.v1.VamfJwtClaimsConstants;
import gov.va.vamf.security.v1.domain.VamfUser;
import gov.va.vamf.security.v1.filters.JwtResourceRestricted;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

/**
 * REST endpoint that allows clients to retrieve Patient Information.
 *
 * @since 1.0
 */
@Component
@JwtResourceRestricted
@Path("/patients/{icn}")
public class ServiceResource {

    /**
     * Returns the Patient Information {@link PatientInfoResponse} associated with the provided ICN Identifier.
     *
     * @param icn     the ICN Identifier used to retrieve the {@link PatientInfoResponse}
     * @param headers the {@link HttpHeaders} object used to retrieve the current request JWT header value
     * @return the Patient Information {@link PatientInfoResponse}
     * @see PatientInfoResponse
     * @see HttpHeaders
     */
    @GET
    @Path("/info")
    @Produces(MediaType.APPLICATION_JSON)
    public PatientInfoResponse getPatientInfo(@PathParam("icn") final String icn,
                                              @Context final HttpHeaders headers) {
        final String jwt = headers.getHeaderString(VamfJwtClaimsConstants.VAMF_JWT_HEADER);
        final VamfUser user = new VamfUser(jwt);

        final PatientInfo info = new PatientInfo();
        info.setIcn(user.getPatient().getIcn());
        info.setFirstName(user.getPatient().getFirstName());
        info.setLastName(user.getPatient().getLastName());
        info.setDateOfBirth(user.getPatient().getDateOfBirth());

        final PatientInfoResponse response = new PatientInfoResponse();
        response.setData(info);

        return response;
    }
}