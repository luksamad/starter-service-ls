package gov.va.mobile.starter.v1.service.proxies;

import gov.va.mobile.starter.v1.service.config.EnvConfig;
import gov.va.mobile.starter.v1.service.exceptions.ApiException;
import gov.va.mobile.starter.v1.service.exceptions.ErrorCode;
import gov.va.vamf.session.v1.ApiClient;
import gov.va.vamf.session.v1.client.SessionApi;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Service
@Log4j2
@SuppressWarnings({"PMD.GuardLogStatement"})
public class SessionServiceClient {

    private final SessionApi sessionApi;

    public SessionServiceClient(final EnvConfig config, final RestTemplate restTemplate) {
        ApiClient apiClient = new ApiClient(restTemplate);
        apiClient.setBasePath(config.getUserSessionServiceUrl());
        sessionApi = new SessionApi(apiClient);
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    @NewSpan("SessionServiceClient:getSessionVariable")
    public String getSessionVariable(final String jwt, final String sessionVariable) {
        sessionApi.getApiClient().setApiKey(jwt);
        try {
            return sessionApi.getSessionValue(sessionVariable);
        } catch (RestClientResponseException e) {
            if (e.getRawStatusCode() == 404) {
                log.debug("404 Failed to get session variable from session API");
                return null;
            } else {
                log.warn("Failed to get session variable from session API");
                throw new ApiException(ErrorCode.SESSION_SVC_FAILED_TO_READ_SESSION_VARIABLE, e);
            }
        } catch (Exception e) {
            throw new ApiException(ErrorCode.SESSION_SVC_FAILED_TO_READ_SESSION_VARIABLE, e);
        }
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    @NewSpan("SessionServiceClient:putSessionVariable")
    public void putSessionVariable(final String jwt, final String sessionVariable, final String value) {
        sessionApi.getApiClient().setApiKey(jwt);
        try {
            sessionApi.setSessionValue(sessionVariable, value);
        } catch (RestClientResponseException e) {
            String message = String.format("%s %s", "Failed to store data in user session variable", sessionVariable);
            log.warn("{}: {}", message, e.getResponseBodyAsString());
            throw new ApiException(ErrorCode.SESSION_SVC_FAILED_TO_STORE_SESSION_VARIABLE, e);
        } catch (Exception e) {
            log.warn("Failed to store session variable in session API.");
            throw new ApiException(ErrorCode.SESSION_SVC_FAILED_TO_STORE_SESSION_VARIABLE, e);
        }
    }
}
