package gov.va.mobile.starter.v1.service.exceptions;

import org.springframework.lang.Nullable;

public enum ErrorCode {

    // User Session Service failures
    SESSION_SVC_FAILED_TO_READ_SESSION_VARIABLE(12_000, 500, "Failed to read data in user session variable"),
    SESSION_SVC_FAILED_TO_STORE_SESSION_VARIABLE(12_001, 500, "Failed to store data in user session variable");

    private final int value;
    private final int httpStatus;
    private final String message;

    ErrorCode(final int value, final int httpStatus, final String message) {
        this.value = value;
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public static ErrorCode valueOf(final int errorCode) {
        ErrorCode error = resolve(errorCode);
        if (error == null) {
            throw new IllegalArgumentException("No matching constant for [" + errorCode + "]");
        }
        return error;
    }

    @Nullable
    public static ErrorCode resolve(final int errorCode) {
        for (ErrorCode error : values()) {
            if (error.value == errorCode) {
                return error;
            }
        }
        return null;
    }

    public int value() {
        return this.value;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return this.value + " " + name();
    }
}