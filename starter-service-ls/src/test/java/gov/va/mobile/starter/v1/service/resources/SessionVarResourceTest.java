package gov.va.mobile.starter.v1.service.resources;

import gov.va.mobile.starter.v1.service.config.EnvConfig;
import gov.va.mobile.starter.v1.service.exceptions.ApiException;
import gov.va.mobile.starter.v1.service.exceptions.ErrorCode;
import gov.va.mobile.starter.v1.service.proxies.SessionServiceClient;
import gov.va.vamf.session.v1.client.SessionApi;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestClientResponseException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class SessionVarResourceTest {

    private static final String SESSION_VAR = "TEST" + RandomUtils.nextInt();

    @InjectMocks
    SessionServiceClient client;

    @Mock
    SessionApi sessionApi;

    @Mock
    EnvConfig config;

    @Mock
    RestClientResponseException restClientResponseException;


    @Before
    public void setUp() throws Exception {
        ReflectionTestUtils.setField(client, "sessionApi", sessionApi);
    }


    @Test
    public void getSessionVariableTest() {
        when(sessionApi.getApiClient()).thenReturn(new gov.va.vamf.session.v1.ApiClient());
        when(sessionApi.getSessionValue(SESSION_VAR)).thenReturn("some_value");
        String result = client.getSessionVariable("jwt", SESSION_VAR);
        assertThat(result).isEqualTo("some_value");
    }

    @Test
    public void getSessionVariableTest_catchRestClientException404() {
        when(sessionApi.getApiClient()).thenReturn(new gov.va.vamf.session.v1.ApiClient());
        RestClientResponseException restClientResponseException = new RestClientResponseException("error", 404,
                "sessionApi Error", null, null, null);
        when(sessionApi.getSessionValue(SESSION_VAR)).thenThrow(restClientResponseException);
        String result = client.getSessionVariable("jwt", SESSION_VAR);
        assertThat(result).isNull();
    }

    @Test
    public void getSessionVariableTest_throwsSessionServiceException() {
        when(sessionApi.getApiClient()).thenReturn(new gov.va.vamf.session.v1.ApiClient());
        RestClientResponseException restClientResponseException = new RestClientResponseException("error", 500,
                "sessionApi Error", null, null, null);
        when(sessionApi.getSessionValue(SESSION_VAR)).thenThrow(restClientResponseException);
        Throwable thrown = catchThrowable(() -> client.getSessionVariable("jwt", SESSION_VAR));
        assertThat(thrown).isInstanceOf(ApiException.class);
    }

    @Test
    public void getSessionVariableTest_throwsSessionServiceException_catchGenericException() {
        when(sessionApi.getApiClient()).thenReturn(new gov.va.vamf.session.v1.ApiClient());
        ApiException apiException = new ApiException(ErrorCode.SESSION_SVC_FAILED_TO_READ_SESSION_VARIABLE, restClientResponseException);
        when(sessionApi.getSessionValue(SESSION_VAR)).thenThrow(apiException);
        Throwable thrown = catchThrowable(() -> client.getSessionVariable("jwt", SESSION_VAR));
        assertThat(thrown).isInstanceOf(ApiException.class);
    }

    @Test
    public void putSessionVariableTest_Success() {
        when(sessionApi.getApiClient()).thenReturn(new gov.va.vamf.session.v1.ApiClient());
        client.getSessionVariable("jwt", SESSION_VAR);
    }

    @Test
    public void putSessionVariableTest_throwsSessionServiceException() {
        when(sessionApi.getApiClient()).thenReturn(new gov.va.vamf.session.v1.ApiClient());
        RestClientResponseException restClientResponseException = new RestClientResponseException("error", 500,
                "sessionApi Error", null, null, null);
        doThrow(restClientResponseException).when(sessionApi).setSessionValue(SESSION_VAR, "some_value");
        Throwable thrown = catchThrowable(() -> client.putSessionVariable("jwt", SESSION_VAR, "some_value"));
        assertThat(thrown).isInstanceOf(ApiException.class);
    }

    @Test
    public void putSessionVariableTest_throwsSessionServiceException_catchGenericException() {
        when(sessionApi.getApiClient()).thenReturn(new gov.va.vamf.session.v1.ApiClient());
        ApiException apiException =
                new ApiException(ErrorCode.SESSION_SVC_FAILED_TO_STORE_SESSION_VARIABLE, restClientResponseException);
        doThrow(apiException).when(sessionApi).setSessionValue(SESSION_VAR, "some_value");
        Throwable thrown = catchThrowable(() -> client.putSessionVariable("jwt", SESSION_VAR, "some_value"));
        assertThat(thrown).isInstanceOf(ApiException.class);
    }
}