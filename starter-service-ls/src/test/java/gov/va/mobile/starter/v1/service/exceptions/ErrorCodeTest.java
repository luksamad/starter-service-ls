package gov.va.mobile.starter.v1.service.exceptions;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ErrorCodeTest {
    @Test
    public void testErrorCode() {
        ErrorCode errorCode = ErrorCode.resolve(12_000);
        assertThat(errorCode.getMessage().equals("Failed to read data in user session variable")).isTrue();
        assertThat(errorCode.getHttpStatus() == 500).isTrue();
        assertThat(errorCode.toString().equals("12000 SESSION_SVC_FAILED_TO_READ_SESSION_VARIABLE")).isTrue();
        assertThat(ErrorCode.valueOf(12_000).equals(errorCode)).isTrue();
    }

    @Test
    public void testErrorCodeTwo() {
        ErrorCode errorCode = ErrorCode.resolve(333333);
        assertThat(errorCode == null);

        try {
            ErrorCode.valueOf(532409324);
        } catch (IllegalArgumentException e) {
            assertThat(e instanceof IllegalArgumentException);
        }
    }
}
