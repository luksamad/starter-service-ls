package gov.va.mobile.starter.v1.service.resources;

import gov.va.mobile.starter.v1.ApiClient;
import gov.va.mobile.starter.v1.auth.ApiKeyAuth;
import gov.va.mobile.starter.v1.client.SessionVariableApi;
import gov.va.mobile.starter.v1.client.model.SessionVar;
import gov.va.mobile.starter.v1.service.BaseITCase;
import gov.va.vamf.security.v1.domain.VamfUser;
import gov.va.vamf.security.v1.utility.UserToJwt;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class SessionVarResourceITCase extends BaseITCase {

    static SessionVar TEST_VALUE = new SessionVar();

    static {
        TEST_VALUE.setValue("TEST" + RandomUtils.nextInt());
    }

    @Test
    public void testAllGoodInTheHood() {
        final String ICN = "2222222222";
        final VamfUser user = createTestUser(ICN);
        user.setAuthorizedResources(List.of("/user/sessionvar"));
        final SessionVariableApi api = getWithJwtApi(user);

        ApiKeyAuth apiKeyAuth = (ApiKeyAuth) api.getApiClient().getAuthentication("requireVamfJwtKey");
        String jwt = apiKeyAuth.getApiKey();
        assertThat(jwt).isNotNull();

        List<String> keys = Arrays.asList("TEST1", "TEST2", "TEST3", "TEST4", "TEST5", "TEST6");
        for (String key : keys) {
            api.updateSessionVariable(key, TEST_VALUE);
            String keyValue = api.getSessionVariable(key);
            assertThat(keyValue).isEqualTo(TEST_VALUE.getValue());
        }
    }

    @Test
    public void testBadJwtUnauthorized() {
        final String ICN = "2222222222";
        final VamfUser user = createTestUser(ICN);
        user.setAuthorizedResources(List.of("/user/sessionvar"));
        final SessionVariableApi api = getWithJwtApi(user);

        ApiKeyAuth apiKeyAuth = (ApiKeyAuth) api.getApiClient().getAuthentication("requireVamfJwtKey");
        String jwt = apiKeyAuth.getApiKey();
        assertThat(jwt).isNotNull();

        api.getApiClient().setApiKey(jwt.substring(0, jwt.length() - 30));

        try {
            api.updateSessionVariable("BAD_BAD_BAD", TEST_VALUE);
        } catch (Exception e) {
            assertThat(e).isInstanceOf(HttpClientErrorException.Unauthorized.class);
            assertThat(e.getMessage()).startsWith("401 Unauthorized");
        }

        try {
            api.getSessionVariable("BAD_BAD_BAD");
        } catch (Exception e) {
            assertThat(e).isInstanceOf(HttpClientErrorException.Unauthorized.class);
            assertThat(e.getMessage()).startsWith("401 Unauthorized");
        }
    }

    @Test
    public void testBadNoJwt() {
        SessionVariableApi api = getWithOutJwtApi();
        try {
            api.updateSessionVariable("BAD_DATA", TEST_VALUE);
        } catch (Exception e) {
            assertThat(e).isInstanceOf(HttpClientErrorException.class);
        }
    }

    @Test
    public void testBadKey() {
        final String ICN = "2222222222";
        final VamfUser user = createTestUser(ICN);
        user.setAuthorizedResources(List.of("/user/sessionvar"));
        final SessionVariableApi api = getWithJwtApi(user);

        ApiKeyAuth apiKeyAuth = (ApiKeyAuth) api.getApiClient().getAuthentication("requireVamfJwtKey");
        String jwt = apiKeyAuth.getApiKey();
        assertThat(jwt).isNotNull();

        api.getApiClient().setApiKey(jwt.substring(0, jwt.length() - 30));

        try {
            api.updateSessionVariable("BAD", null);
        } catch (Exception e) {
            assertThat(e.getMessage()).isEqualTo("400 Missing the required parameter 'sessionVar' when calling updateSessionVariable");
        }

        try {
            api.getSessionVariable(null);
        } catch (Exception e) {
            assertThat(e.getMessage()).isEqualTo("400 Missing the required parameter 'key' when calling getSessionVariable");
        }
    }

    private static SessionVariableApi getWithJwtApi(final VamfUser user) {
        final ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(SERVICE_URL);
        apiClient.setApiKey(generateJwt(user));
        return new SessionVariableApi(apiClient);
    }

    private static SessionVariableApi getWithOutJwtApi() {
        final ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(SERVICE_URL);
        return new SessionVariableApi(apiClient);
    }

    private static String generateJwt(final VamfUser user) {
        return UserToJwt.getJwtForVamfUser(user, JWT_SIGNER).serialize();
    }

    private static VamfUser createTestUser(final String icn) {
        final VamfUser user = new VamfUser("UNKNOWN", "Session", "John", "Variable");
        user.setAuthenticated(true);
        final VamfUser.Patient patient = new VamfUser.Patient();
        patient.setIcn(icn);
        patient.setGender("male");
        patient.setDob("04 JUL 1962");
        patient.setFirstName(user.getFirstName());
        patient.setMiddleName(user.getMiddleName());
        patient.setLastName(user.getLastName());
        patient.setStatus("status");
        user.setPatient(patient);
        user.setIdType("ICN");
        user.setId(icn);
        user.setAuthorizedRoles(Arrays.asList("Staff"));
        return user;
    }


}