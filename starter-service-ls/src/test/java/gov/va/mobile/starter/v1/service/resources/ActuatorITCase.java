package gov.va.mobile.starter.v1.service.resources;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import gov.va.mobile.starter.v1.service.BaseITCase;
import gov.va.vamf.security.v1.VamfJwtClaimsConstants;
import gov.va.vamf.security.v1.domain.VamfUser;
import gov.va.vamf.security.v1.utility.UserToJwt;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

/**
 * Integration Test cases for Actuator Endpoints.
 *
 * @since 1.0
 */
public class ActuatorITCase extends BaseITCase {

    private final RestTemplate restTemplate = new RestTemplate();

    private static String getBasePath() {
        final URI url = URI.create(SERVICE_URL);
        return url.getPath();
    }

    private static String generateJwt(final List<String> authorizedResources) {
        final VamfUser user = createTestUser("111111111");
        if (authorizedResources != null) {
            user.setAuthorizedResources(authorizedResources);
        }

        return UserToJwt.getJwtForVamfUser(user, JWT_SIGNER).serialize();
    }

    private static VamfUser createTestUser(final String icn) {
        final VamfUser user = new VamfUser("123", "User", "Integration", "Test");
        user.setAuthenticated(true);
        final VamfUser.Patient patient = new VamfUser.Patient();
        patient.setIcn(icn);
        patient.setGender("male");
        patient.setDob("04 JUL 1962");
        patient.setFirstName(user.getFirstName());
        patient.setMiddleName(user.getMiddleName());
        patient.setLastName(user.getLastName());
        patient.setStatus("status");
        user.setPatient(patient);
        user.setIdType("ICN");
        user.setId(icn);
        return user;
    }

    /**
     * Make sure health check can be accessed and all health check endpoints are available and 'UP'.
     */
    @Test
    public void testHealthCheck() {
        final String jwt = generateJwt(List.of(getBasePath() + "/system/health"));

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.set(VamfJwtClaimsConstants.VAMF_JWT_HEADER, jwt);
        final HttpEntity<String> entity = new HttpEntity<>(headers);

        final String url = SERVICE_URL + "/system/health";

        final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull().isNotEmpty();

        // Parse and validate the JSON response
        final Object document = Configuration.defaultConfiguration().jsonProvider().parse(response.getBody());
        String jsonVar = JsonPath.read(document, "$.status");
        assertThat(jsonVar).isEqualTo("UP");

        // Check that Example-Service is available and healthy
        jsonVar = JsonPath.read(document, "$.components.exampleService.status");
        assertThat(jsonVar).isEqualTo("UP");
    }

    /**
     * Make sure health check cannot be access unless it is grated access in the JWT.
     */
    @Test
    public void testHealthCheckWithoutResourceGrant() {
        // Do NOT give access to /system/health
        final String jwt = generateJwt(List.of(getBasePath() + "/something/else"));

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.set(VamfJwtClaimsConstants.VAMF_JWT_HEADER, jwt);
        final HttpEntity<String> entity = new HttpEntity<>(headers);

        final String url = SERVICE_URL + "/system/health";

        final Throwable thrown = catchThrowable(() -> restTemplate.exchange(url, HttpMethod.GET, entity, String.class));

        assertThat(thrown).isInstanceOf(HttpClientErrorException.class);
        if (thrown instanceof HttpClientErrorException) {
            final HttpClientErrorException error = (HttpClientErrorException) thrown;
            assertThat(error.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        }
    }

    /**
     * Verify that the /system/health endpoint is not accessible without a JWT.
     */
    @Test
    public void testHealthCheckWithoutJwt() {
        final String url = SERVICE_URL + "/system/health";
        final Throwable thrown = catchThrowable(() -> restTemplate.getForEntity(url, String.class));

        assertThat(thrown).isInstanceOf(HttpClientErrorException.class);
        if (thrown instanceof HttpClientErrorException) {
            final HttpClientErrorException error = (HttpClientErrorException) thrown;
            assertThat(error.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * Verify that the /system/k8s/readiness endpoint can be accessed with a JWT even if the resource authorization is
     * not present.
     */
    @Test
    public void testK8sReadinessWithJwt() {
        final String jwt = generateJwt(List.of(getBasePath() + "/system/.*"));

        final HttpHeaders headers = new HttpHeaders();
        headers.set(VamfJwtClaimsConstants.VAMF_JWT_HEADER, jwt);
        final HttpEntity<String> entity = new HttpEntity<>(headers);

        final String url = SERVICE_URL + "/system/k8s/readiness";

        final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    /**
     * Verify that the /system/k8s/readiness endpoint can be accessed without a JWT.
     */
    @Test
    public void testK8sReadinessWithoutJwt() {
        final String url = SERVICE_URL + "/system/k8s/readiness";
        final ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    /**
     * Verify that the /system/k8s/prometheus endpoint can be accessed with a JWT even if the resource authorization is
     * not present.
     */
    @Test
    public void testK8sPrometheusWithJwt() {
        final String jwt = generateJwt(List.of(getBasePath() + "/system/.*"));

        final HttpHeaders headers = new HttpHeaders();
        headers.set(VamfJwtClaimsConstants.VAMF_JWT_HEADER, jwt);
        final HttpEntity<String> entity = new HttpEntity<>(headers);

        final String url = SERVICE_URL + "/system/k8s/prometheus";

        final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull().isNotEmpty();
    }

    /**
     * Verify that the /system/k8s/prometheus endpoint can be accessed without a JWT.
     */
    @Test
    public void testK8sPrometheusWithoutJwt() {
        final String url = SERVICE_URL + "/system/k8s/prometheus";
        final ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull().isNotEmpty();
    }
}