package gov.va.mobile.starter.v1.service;

import gov.va.vamf.security.v1.utility.JwtSigner;
import gov.va.vamf.tool.K8sServiceValues;
import lombok.extern.log4j.Log4j2;
import org.junit.BeforeClass;

/**
 * Base Class for the starter-service-ls Integration Test Cases.
 *
 * @since 1.0
 */
@Log4j2
public abstract class BaseITCase {
    protected static final String SERVICE_URL = K8sServiceValues.K8S_SERVICE_URL.getValue();
    private static final String JWT_PUBLIC_KEY = System.getProperty("JWT_PUBLIC_KEY");
    private static final String JWT_PRIVATE_KEY = System.getProperty("JWT_PRIVATE_KEY");
    protected static final JwtSigner JWT_SIGNER = new JwtSigner(JWT_PUBLIC_KEY, JWT_PRIVATE_KEY);

    /**
     * Log the test environment variables.
     */
    @BeforeClass
    public static void init() {
        log.info("Using {} to connect to the starter-service-ls", SERVICE_URL);
    }
}
