package gov.va.mobile.starter.v1.service.exceptions;

import org.junit.Assert;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ApiExceptionTest {

    @Test
    public void testApiExceptionErrorCode1Test() {
        ErrorCode errorCode = ErrorCode.resolve(12_000);
        assertThat(ErrorCode.valueOf(12_000).equals(errorCode)).isTrue();
        ApiException apiException = new ApiException(errorCode, new Exception("Coding for coverage"), "Coding for coverage");
        Assert.assertNotNull(apiException);
        Assert.assertNotNull(apiException.getMessage());
        Assert.assertNotNull(apiException.getErrorCode());
        Assert.assertNotNull(apiException.getStackTrace());
    }

    @Test
    public void testApiExceptionErrorCode2Test() {
        ErrorCode errorCode = ErrorCode.resolve(12_000);
        assertThat(ErrorCode.valueOf(12_000).equals(errorCode)).isTrue();
        ApiException apiException = new ApiException(errorCode, new Exception("Coding for coverage"));
        Assert.assertNotNull(apiException);
        Assert.assertNotNull(apiException.getMessage());
        Assert.assertNotNull(apiException.getErrorCode());
        Assert.assertNotNull(apiException.getStackTrace());
    }
}
