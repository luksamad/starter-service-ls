package gov.va.mobile.starter.v1.service.resources;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.nimbusds.jwt.JWTClaimsSet;
import gov.va.mobile.starter.v1.ApiClient;
import gov.va.mobile.starter.v1.auth.ApiKeyAuth;
import gov.va.mobile.starter.v1.client.StarterApi;
import gov.va.mobile.starter.v1.client.model.PatientInfo;
import gov.va.mobile.starter.v1.client.model.PatientInfoResponse;
import gov.va.mobile.starter.v1.service.BaseITCase;
import gov.va.vamf.security.v1.domain.VamfUser;
import gov.va.vamf.security.v1.utility.JwtUtil;
import gov.va.vamf.security.v1.utility.UserToJwt;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration Test cases for the {@link ServiceResource} REST endpoint.
 *
 * @since 1.0
 */
public class ServiceResourceITCase extends BaseITCase {


    private static StarterApi getApi(final VamfUser user) {
        final ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(SERVICE_URL);
        apiClient.setApiKey(generateJwt(user));
        return new StarterApi(apiClient);
    }

    private static String generateJwt(final VamfUser user) {
        return UserToJwt.getJwtForVamfUser(user, JWT_SIGNER).serialize();
    }

    private static VamfUser createTestUser(final String icn) {
        final VamfUser user = new VamfUser("UNKNOWN", "Public", "John", "Quincy");
        user.setAuthenticated(true);
        final VamfUser.Patient patient = new VamfUser.Patient();
        patient.setIcn(icn);
        patient.setGender("male");
        patient.setDob("04 JUL 1962");
        patient.setFirstName(user.getFirstName());
        patient.setMiddleName(user.getMiddleName());
        patient.setLastName(user.getLastName());
        patient.setStatus("status");
        user.setPatient(patient);
        user.setIdType("ICN");
        user.setId(icn);
        return user;
    }

    /**
     * Make sure we can retrieve the Patient Information.
     */
    @Test
    public void getPatientInfo() {
        final String ICN = "2222222222";
        final VamfUser user = createTestUser(ICN);
        user.setAuthorizedResources(List.of(String.format("patients/%s/info", ICN)));

        final StarterApi api = getApi(user);
        final PatientInfoResponse response = api.getPatientInfo(ICN);
        assertThat(response).isNotNull();

        ApiKeyAuth apiKeyAuth = (ApiKeyAuth) api.getApiClient().getAuthentication("requireVamfJwtKey");
        String jwt = apiKeyAuth.getApiKey();
        assertThat(jwt).isNotNull();

        JWTClaimsSet claimsSet = JwtUtil.getClaimsSetFromJwt(jwt);
        JSONObject patient = (JSONObject) claimsSet.getClaim("patient");
        assertThat(patient.get("icn")).isEqualTo(user.getPatient().getIcn());

        PatientInfo patientInfo = response.getData();
        assertThat(patientInfo).isNotNull();
        assertThat(patientInfo.getIcn()).isEqualTo(ICN);
        assertThat(patientInfo.getLastName()).isEqualTo("Public");
        assertThat(patientInfo.getFirstName()).isEqualTo("John");
    }
}