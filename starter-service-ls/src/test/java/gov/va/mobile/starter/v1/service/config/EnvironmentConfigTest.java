package gov.va.mobile.starter.v1.service.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class EnvironmentConfigTest {

    @Test
    public void testLoadConfiguration() {
        try {
            new EnvConfig();
        } catch (IllegalArgumentException e) {
            assertThat(e instanceof IllegalArgumentException);
        }
    }
}