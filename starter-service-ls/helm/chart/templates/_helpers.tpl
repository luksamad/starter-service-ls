{{- define "probe.command" -}}
exec:
  command:
  {{- /* splits space-delimited command into expected YAML array */}}
  {{- range (splitList " " .command) }}
  - {{ . }}
  {{- end }}
{{- end -}}

{{- define "probe.http" -}}
httpGet:
  path: {{ .httpPath | quote}}
  port: {{ .httpPort }}
{{- end -}}

{{- define "probe.tcpSocket" -}}
tcpSocket:
  port: {{ .tcpSocketPort }}
{{- end -}}

{{- define "getEnvValues" }}
{{- /* if the envConfigs block, return original values merged in with envConfigs specific to environment */ -}}
{{- if and (.Values.envConfigs | not | empty) (.Values.global.environment | not | empty) }}
{{-   toYaml (mergeOverwrite .Values (index .Values.envConfigs .Values.global.environment)) }}
{{- else }}
{{- /* default is to return default values */ -}}
{{-   toYaml .Values }}
{{- end }}
{{- end -}}


{{- define "pathify" }}
{{- if . }}
{{- print . "/" }}
{{- else }}
{{- "" }}
{{- end }}
{{- end -}}