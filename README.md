# starter-service-ls v1

## Summary

The starter-service-ls provides crucial functionality.

[//]: # (TODO: expand summary)

## Dependencies / Data Sources

| Service | Version | Description                                   |
|:--------|:--------|:----------------------------------------------|
|mobile-mvi-service|1.1.0| Mock mvi server for integration test purposes |

## Service Interface

* [Swagger Document](__rootArtifactId__-service/src/main/resources/swagger.json)

## Deploy to Kubernetes

* [Deployment Steps](__rootArtifactId__-service/docs/deployment.md)

### Health Check APIs

This service leverages the VAMF Base Jersey image, so it inherits the Spring Actuator APIs described by the
[VAMF Service Base Jersey project](https://coderepo.mobilehealth.va.gov/projects/VAMFC/repos/service-base-jersey/browse/README.md)
.

The starter-service-ls health check endpoint is available at:

`/starter/v1/system/health` -> Requires a JWT token with a resource grant of `system/.*`

## Local Development / Testing Setup

### From Source

#### Prerequisites:

##### 1. Maven Settings

Make sure you have VA Nexus credentials stored in your `~/.m2/settings.xml` file for `ckm-releases`

    <server>
      <id>ckm-releases</id>
      <username>{va-nexus-username}</username>
      <password>{va-nexus-password}</password>
    </server>

##### 2. Docker Trusted Registry (DTR) Setup

You must have access to
the [MAP Sandbox environment](https://wiki.mobilehealth.va.gov/pages/viewpage.action?pageId=83957173). You should
run `docker login` to ensure that you can access images during the build process.

     $ docker login dtr.mapsandbox.net

#### Maven Build:

Docker Maven Plugin (DMP) is used to run manage the standup and teardown of docker containers for integration tests. To
run starter-service-ls locally, run:

    $ mvn package docker:run 

After a couple of minutes the service should be running on a dynamic port.
