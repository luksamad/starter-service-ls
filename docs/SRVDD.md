starter-service-ls Service Design Document (SRVDD)
===============================================

# Service Information

* Repository: "insert URL to coderepo"
* Major Release: 1.x

*Replace this text with - Describe the functionality of the service to include a high-level description of the overall
use of the service.*

*Replace this text with a description of the changes in this major release over the previous major release. If this is
version 1.x, just say "This is the initial release of the service."*

- [x] Veteran (Internet Accessible)
- [ ] Staff

Note - if the data comes from another service, DB Type and DB Name can be N/A, otherwise, they must be filled in if this
service accesses the data directly. If the data is stored in a DB, then VAMF/VA source is N/A. Any data listed here in a
local repository (directly accessible) must be described in the persistence models.

| **Data Type** | **VAMF/VA Source** | **CRUD Operations** | **DB Type** | **DB Name** | **Data Classification** | **
Reason / Requirement** |
|:---|:---:|:---:|:---:|:---:|:---:|:---:|
|*English description of data*| *service name if it comes from a service* | Letters - C,R,U,D | *Oracle,Mongo,etc*| *DB
name in environment*| *PHI, PII, Non-sensitive, Sensitive (like a key of some kind)* |*English explanation of why app
uses this data* |

*Example of completed table: This example is of a composite service. It reads other services, but stores data in a
cache. It is not intended to be 100% accurate to the service example*

| **Data Type** | **VAMF/VA Source** | **CRUD Operations** | **DB Type** | **DB Name** | **Data Classification** | **
Reason / Requirement** |
|:---|:---:|:---:|:---:|:---:|:---:|:---:|
|Vista Appointments | vista-scheduling-service | R | N/A | N/A | PHI | Retrieve Vista appointments |
|Video Visits | video-visit-service | R |  N/A | N/A | PHI | Retrieve video appointments |
|Clinic data | cdw-service-v3 |R| N/A | N/A | Non-sensitive | Retrieve administrative data |
|Patient cache | N/A | CRUD | redis | appointments-{session-id} | PHI | Stores previously retrieved data in a cache |

# Conceptual Design
-----------------
*Replace this text with high-level bullets describing the conceptual architecture*

- *Replace text*
- *Replace text*

*Replace this with text describing a diagram.*

*Replace this with a high-level context diagram that shows all of the external interfaces this service has. The diagram
should include all services called, including app-specific services, MAP shared services, and VA enterprise services.
Only show this service and its direct interfaces.*

*For data stored with this service, show an entity-relationship diagram of the data. Not all attributes depicted on the
diagram. The physical model will be required at the completion of development.*

# Logical Design Perspective

Swagger definition: *Replace this with a link to the swagger document for this service.*

*Replace this text with sequence diagrams that covers all the interfaces with components outside of this SRVDD. A
sequence diagram should not just be one line but should be a scenario such as create an appointment or update the user's
weight.  
The scenario can cover more than one external interface such as your app-specific service calls along with cdw-service (
as an example of a shared service). All external services called by the service must appear in at least one sequence
diagram.*

*Each endpoint in the swagger should be mapped to the external components it accesses. Keep in mind that database
systems are external, other Mobile shared services are external, and other VA enterprise services are external. Complete
the table below.*

|Endpoint|HTTP Action|External System Endpoint (table mapping for databases)|Description|
|---|---|---|---|
|/service/v1/appointment/{id}|GET<br/>POST<br/>DELETE|Mongo-appointmentdb|Retrieve, insert, or delete data from appointmentdb collection|
|/service/v1/xxxx/|GET|GET /enrollment/v3/associations<br/>appointmentdb-GET|Return contact information with appointment data|

| **Question** | **YES** | **NO** | **Comment - if answer is no, please explain** |
|:---|:---:|:---:|:---:|
| Are you using any technologies that are not on this page<br> https://wiki.mobilehealth.va.gov/display/DevHelp/Tech+Stack? | [ ]  | [ ]  |  |
| Are you using dependency management software?<br>example, maven, gradle, etc. | [ ]  | [ ]  |  |

**Additions / Deviations from Tech Stack**
*Please list here major technology not on the standard list*

Note - see the criteria in https://wiki.mobilehealth.va.gov/display/VAMFAT/VAMF+Architecture+Team+Reviewer+Perspectives
for expectations on dependencies

| **Name** | **Major Version Planned** | **Backlog item** | **Planned Date to Prod**|
|:---|:---:|:---:|:---:|
| *tech 1* | *version*  | *JIRA issue if applicable* | *xx/xx/xxxx* |
| *tech 2* | *version*  | *JIRA issue if applicable* | *xx/xx/xxxx* |
| *tech 3* | *version*  | *JIRA issue if applicable* | *xx/xx/xxxx* |

*Diagram showing more detailed view of the ER diagram shown after Sprint 0. This is not required for architecture
approval at Sprint 0 timeframe.*

*Replace this with a description of how app service endpoints are secured (JWT, resource based, etc)*

# Physical Design Perspective :

Dockerfile: *replace with link to dockerfile if it has one or provide a link to base image used directly if no
Dockerfile for this service.*

*deployment diagram should only display applicaiton components being deployed and their respective cluster or other
archtiecture boundary.  
e.g. CDW would be an externally represented from other components deployed to K8.*

*Replace with a complete data dictionary for the current database design of a product ready for deployment to Production
grouped by datasource and titled with object name containing Element Name, Data Type, Relationship ('P'=primary key, '
F'=foreign key, 'O'=object '-' = none), and Description/DOV List.*

*Examples below includes required elements*

Object Name:  MyTable (datasource1)

| **Element Name** | **Data Type** | **Relationship** | **Description/DOV List** | |:---|:---:|:---:|:---:|:---:| | *
myID* | *Int(32)*  | *P* | *Incrementing integer as record primary key value* | | *resultCode* | *Varchar(32)*  | *F*
| *Result action logged per DOV{Called, Response, Initial, Final, Closed}* | | *notes* | *Varchar(200)*  | *-* | *Notes
corresponding to action results* |

Object Name:  MyNoSQLCollection (datasource2)

| **Element Name** | **Data Type** | **Relationship** | **Description/DOV List** | |:---|:---:|:---:|:---:|:---:| | *_
id* | *string*  | *P* | *Default MongoDB GUID document identifier* | | *operatorID* | *integer*  | *-* | *Contains
operator ID correlating to OperatorCollection document* | | *currentStatus* | *string*  | *-* | *Current status of
operator on project per DOV{Active, Inactive, Onboarding}* | | *projectRoles* | *array (Roles)*  | *O* | *Array of Roles
held* |

Object Name:  Roles (datasource2)

| **Element Name** | **Data Type** | **Relationship** | **Description/DOV List** | |:---|:---:|:---:|:---:|:---:| | *
role* | *string*  | *-* | *Role name of role held* | | *starDate* | *date*  | *-* | *UTC date assigned* | | *endDate*
| *date*  | *-* | *UTC date removed* |

# Developer & PM Contact Information :

|**Title** | **Name**  | **Point of Contact (VA Email)** | **Phone Number** | **Contract Start Date** | **Contract End
Date** |
|---|:---:|:---:|---|---|---|
|**Tech Lead**|Tech Lead|Tech Lead email||5/29/19|7/5/19|
|**PM**|PM Name|PM Email||5/29/19|7/5/19|
|**Business Owner**|BO Name|BO Email||5/29/19|7/5/19|
